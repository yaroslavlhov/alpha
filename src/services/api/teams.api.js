import http from '@/services/http.service';

export default {
	async getTeams() {
		const result = await http.post('/api/teams');
		return result.data.data;
	},

	async getTeamById(teamId) {
		const result = await http.post('/api/teams', { id: teamId });
		return result.data.data;
	},

	async getPlayers(teamId) {
		const result = await http.post('/api/teams/players', { team_id: teamId });
		return result.data.data;
	},

	async getGames(teamId) {
		const result = await http.post('/api/teams/games', { team_one_id: teamId });
		return result.data.data;
	},

	async createGame(payload) {
		const result = await http.post('/api/teams/games/new', payload);
		return result.data.result;
	},
};
