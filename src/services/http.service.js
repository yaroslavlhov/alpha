import axios from 'axios';

const http = axios.create();

http.defaults.baseURL = 'https://alphafx-code-test-api.herokuapp.com/';

// http.interceptors.request.use((config) => config);
// we can add any interceptors here for request and for response too

export default http;
