import Vue from 'vue';
import VueRouter from 'vue-router';
import TeamsPage from '../views/TeamsPage.vue';
import PageNotFound from '../views/PageNotFound.vue';

Vue.use(VueRouter);
const routes = [
	{
		path: '/',
		name: 'teams',
		component: TeamsPage,
	},
	{
		path: '/team/:team_id',
		name: 'team-detail',
		component: () => import(/* webpackChunkName: "about" */ '../views/TeamDetailPage.vue'),
	},
	{ path: '*', component: PageNotFound },
];
const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
});
export default router;
