import vue from 'vue';
import API from '@/services/api';

export default {
	namespaced: true,
	state: {
		teams: {},
		selectedId: null,
	},
	getters: {
		// eslint-disable-next-line arrow-parens
		teams: (state) => state.teams,
		// eslint-disable-next-line arrow-parens
		selectedTeam: (state) => state.teams[state.selectedId],
	},
	mutations: {
		updateTeam(state, team) {
			vue.set(state.teams, team.id, team);
		},
	},
	actions: {
		async getTeams({ commit }) {
			const res = await API.teams.getTeams();
			res.forEach((team) => {
				commit('updateTeam', team);
			});
		},
		async updateSelectedTeamId({ state, commit }, payload) {
			state.selectedId = payload;
			if (!state.teams[state.selectedId]) {
				const teams = await API.teams.getTeamById(state.selectedId);
				commit('updateTeam', teams[0]);
			}
		},
	},
	modules: {},
};
